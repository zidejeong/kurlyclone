const paymentModel = require("../services/payment");

async function paymentCart(req, res) {
	console.log("paymentCart contorller : ", req.body);
	result = await paymentModel.payOut(req.body);
	console.log("payment contorller result : ", result);
	res
		.status(200)
		.json({ payment_seq: result, message: "장바구니가 지불되었습니다!" });
}

async function paymentList(req, res) {
	console.log("paymentList controller : ", req.query);
	result = await paymentModel.payOutList(req.query);
	console.log("paymentList controller result : ", result);
	res.status(200).json({ paymentList: result });
}

async function paymentDetail(req, res) {
	// console.log("paymentDetail controller : ", req.query);
	result = await paymentModel.payOutDetail(req.query);
	res.status(200).json({ paymentDetail: result });
}

async function reorder(req, res) {
	result = await paymentModel.reorderToCart(req.body);
	console.log("reorder controller result : ", result);
	res.status(200).json({ cart_seq: result });
}

module.exports = {
	paymentCart,
	paymentList,
	paymentDetail,
	reorder,
};
