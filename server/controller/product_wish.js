const productWishModel = require("../services/product_wish");

async function getWishList(req, res) {
	//console.log("get : ", req.query);
	const resultList = await productWishModel.getWishList(req.query);
	//console.log("get controller resultGet : ", resultList);
	res.status(200).json({ wishList: resultList });
}

async function addWishProduct(req, res) {
	console.log("add controller : ", req.body);
	const resultWish = await productWishModel.addWishProduct(req.body);
	console.log("add controller resultAdd : ", resultWish);
	res
		.status(200)
		.json({ wish_item_seq: resultWish, message: "찜 상품이 추가되었습니다!" });
}

async function delWishProduct(req, res) {
	console.log("del : ", req.params.seq);
	const resultDel = await productWishModel.delWishProduct(req.params);
	console.log("delWishProduct resultDel : ", resultDel);
	return res.status(200).json({ message: "찜 상품이 삭제되었습니다!" });
}

module.exports = {
	getWishList,
	addWishProduct,
	delWishProduct,
};
