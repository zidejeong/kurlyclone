const dbPool = require("../db");

async function insertCartDetail(item, insertId) {
	const [resultCartDetailInsert] = await dbPool.query(
		`INSERT INTO tb_cart_detail SET
			product_view_seq = ${item.product_view_seq},
			product_seq = ${item.product_seq},
			user_seq = ${item.user_seq},
			cart_seq = ${insertId},
			products_buy_count = ${item.products_buy_count},
			products_total_price = ${item.total_price},
			is_delete = "1",
			create_dtm = now(),
			update_dtm = now()`
	);
	console.log("resultCartDetailInsert db : ", resultCartDetailInsert.insertId);
	return resultCartDetailInsert.insertId;
}

async function insertCart(item) {
	console.log("function cartInsert : ", item);
	try {
		const [resultCartInsert] = await dbPool.query(
			`INSERT INTO tb_cart (
				user_seq,
				total_product_count,
				status,
				cart_total_price,
				delivery_price,
				total_cart_discount_price,
				total_accumulate_price,
				payment_price,
				create_dtm,
				update_dtm)
			SELECT
				${item.user_seq},
				'${item.products_buy_count}',
				'0',
				'${item.total_price}',
				'${item.total_price - item.total_discount_price > 43000 ? 0 : 3000}',
				'${item.total_discount_price}',
				'${item.total_accumulate_price}',
				'${item.total_price - item.total_discount_price}',
				now(),
				now()
			FROM DUAL WHERE NOT EXISTS (
				 SELECT user_seq, status
					 FROM tb_cart
				  WHERE user_seq = ${item.user_seq}
					  AND status = "0");`
		);
		console.log("resultCartInsert : ", resultCartInsert);
		if (resultCartInsert.insertId == 0) {
			const [resultCardId] = await dbPool.query(
				`SELECT cart_seq, payment_price
					FROM tb_cart
				 WHERE user_seq = ${item.user_seq}
				   AND status = "0";`
			);
			console.log("resultCardId [--]: ", resultCardId);
			const resultCartUpdate = await updateCart(
				item,
				resultCardId[0].payment_price
			);
			console.log("<><><> : ", resultCardId[0].cart_seq);
			return resultCardId[0].cart_seq;
		}
		return resultCartInsert.insertId;
	} catch (e) {
		console.error(e);
	}
}

async function updateCartDetail(item, insertId = 0) {
	console.log("function updateCartDetail : ", item, insertId);
	if (item.guest_cart) {
		try {
			for await (let guest of item.guest_cart) {
				console.log("for updateCartDetail : ", guest);
				const [resultProduct] = await dbPool.query(
					`SELECT product_price, discount_price, accumulate_price, product_discount_price
						 FROM tb_product
						WHERE product_seq = ${guest.product_seq}`
				);
				console.log("guest product db result : ", resultProduct[0]);
				resultProduct[0].user_seq = item.user_seq;
				resultProduct[0].product_seq = guest.product_seq;
				resultProduct[0].product_view_seq = guest.product_view_seq;
				resultProduct[0].products_buy_count = guest.products_buy_count;
				resultProduct[0].total_price =
					parseInt(guest.products_buy_count) *
					parseInt(resultProduct[0].product_price);
				resultProduct[0].total_discount_price =
					parseInt(guest.products_buy_count) *
					parseInt(resultProduct[0].discount_price);
				resultProduct[0].total_accumulate_price =
					parseInt(guest.products_buy_count) *
					parseInt(resultProduct[0].accumulate_price);
				console.log("guest product db result >> : ", resultProduct[0]);
				const [resultCartDetailUpdate] = await dbPool.query(
					`UPDATE tb_cart_detail tb1 SET
						tb1.products_buy_count = CONVERT(tb1.products_buy_count, UNSIGNED) + ${parseInt(
							resultProduct[0].products_buy_count
						)},
						tb1.products_total_price = CONVERT(tb1.products_total_price, UNSIGNED) + ${parseInt(
							resultProduct[0].total_price
						)},
						tb1.update_dtm = now()
					WHERE tb1.user_seq = ${resultProduct[0].user_seq}
						AND tb1.product_seq = ${resultProduct[0].product_seq}
						AND tb1.is_delete = "1";`
				);
				console.log("resultCartDetailUpdate db : ", resultCartDetailUpdate);
				if (resultCartDetailUpdate.affectedRows == 0) {
					const resultCartDetailInsert = await insertCartDetail(
						resultProduct[0],
						insertId
					);
					console.log(
						"resultCartDetailInsert db result : ",
						resultCartDetailInsert
					);
					// return resultCartDetailInsert;
				}
				// return resultCartDetailUpdate.info;

				// if (resultCartDetailUpdate.affectedRows == 0) {
				// 	const resultCartDetailInsert = await insertCartDetail(item, insertId);
				// 	console.log(
				// 		"resultCartDetailInsert db result : ",
				// 		resultCartDetailInsert
				// 	);
				// 	return resultCartDetailInsert;
				// }
				// return resultCartDetailUpdate.info;
			}
		} catch (e) {
			console.error(e);
		}
	} else {
		try {
			const [resultCartDetailUpdate] = await dbPool.query(
				`UPDATE tb_cart_detail tb1 SET
					tb1.products_buy_count = CONVERT(tb1.products_buy_count, UNSIGNED) + ${parseInt(
						item.products_buy_count
					)},
					tb1.products_total_price = CONVERT(tb1.products_total_price, UNSIGNED) + ${parseInt(
						item.total_price
					)},
					tb1.update_dtm = now()
				WHERE tb1.user_seq = ${item.user_seq}
					AND tb1.product_seq = ${item.product_seq}
					AND tb1.is_delete = "1";`
			);
			console.log("resultCartDetailUpdate db : ", resultCartDetailUpdate);
			if (resultCartDetailUpdate.affectedRows == 0) {
				const resultCartDetailInsert = await insertCartDetail(item, insertId);
				console.log(
					"resultCartDetailInsert db result : ",
					resultCartDetailInsert
				);
				return resultCartDetailInsert;
			}
			return resultCartDetailUpdate.info;
		} catch (e) {
			console.error(e);
		}
	}
	// const [resultCartDetailUpdate] = await dbPool.query(
	// 	`UPDATE tb_cart_detail tb1 SET
	// 		tb1.products_buy_count = CONVERT(tb1.products_buy_count, UNSIGNED) + ${parseInt(
	// 			item.products_buy_count
	// 		)},
	// 		tb1.products_total_price = CONVERT(tb1.products_total_price, UNSIGNED) + ${parseInt(
	// 			item.total_price
	// 		)},
	// 		tb1.update_dtm = now()
	// 	WHERE tb1.user_seq = ${item.user_seq}
	// 		AND tb1.product_seq = ${item.product_seq}
	// 		AND tb1.is_delete = "1";`
	// );
	// console.log("resultCartDetailUpdate db : ", resultCartDetailUpdate);
	// if (resultCartDetailUpdate.affectedRows == 0) {
	// 	const resultCartDetailInsert = await insertCartDetail(item, insertId);
	// 	console.log("resultCartDetailInsert db result : ", resultCartDetailInsert);
	// 	return resultCartDetailInsert;
	// }
	// return resultCartDetailUpdate.info;
}

async function updateCart(item, price) {
	console.log("function cartUpdate : ", item, price);
	if (item.guest_cart) {
		try {
			for await (let guest of item.guest_cart) {
				console.log("updateCart : ", guest);
				const [resultProduct] = await dbPool.query(
					`SELECT product_price, discount_price, accumulate_price, product_discount_price
						 FROM tb_product
						WHERE product_seq = ${guest.product_seq}`
				);
				console.log("guest product db result : ", resultProduct[0]);
				resultProduct[0].user_seq = item.user_seq;
				resultProduct[0].product_seq = guest.product_seq;
				resultProduct[0].product_view_seq = guest.product_view_seq;
				resultProduct[0].products_buy_count = guest.products_buy_count;
				resultProduct[0].total_price =
					parseInt(guest.products_buy_count) *
					parseInt(resultProduct[0].product_price);
				resultProduct[0].total_discount_price =
					parseInt(guest.products_buy_count) *
					parseInt(resultProduct[0].discount_price);
				resultProduct[0].total_accumulate_price =
					parseInt(guest.products_buy_count) *
					parseInt(resultProduct[0].accumulate_price);
				console.log("guest product db result >> : ", resultProduct[0]);
				const [resultCartUpdate] = await dbPool.query(
					`UPDATE tb_cart tb1 SET
						tb1.total_product_count = CONVERT(tb1.total_product_count, UNSIGNED) + ${parseInt(
							resultProduct[0].products_buy_count
						)},
						tb1.cart_total_price = CONVERT(tb1.cart_total_price, UNSIGNED) + ${parseInt(
							resultProduct[0].total_price
						)},
						tb1.delivery_price = ${
							parseInt(price) +
								parseInt(resultProduct[0].total_price) -
								parseInt(resultProduct[0].total_discount_price) >
							43000
								? 0
								: 3000
						},
						tb1.total_cart_discount_price = CONVERT(tb1.total_cart_discount_price, UNSIGNED) + ${parseInt(
							resultProduct[0].total_discount_price
						)},
						tb1.total_accumulate_price = CONVERT(tb1.total_accumulate_price, UNSIGNED) + ${parseInt(
							resultProduct[0].total_accumulate_price
						)},
						tb1.payment_price = CONVERT(tb1.payment_price, UNSIGNED) + ${
							parseInt(resultProduct[0].total_price) -
							parseInt(resultProduct[0].total_discount_price)
						},
						tb1.update_dtm = now()
					WHERE tb1.user_seq = ${resultProduct[0].user_seq}
						AND tb1.status = "0";`
				);
				console.log("DB resultCartUpdate : ", resultCartUpdate.info);
				// return resultCartUpdate.info;
			}
		} catch (e) {
			console.error(e);
		}
	} else {
		try {
			const [resultCartUpdate] = await dbPool.query(
				`UPDATE tb_cart tb1 SET
					tb1.total_product_count = CONVERT(tb1.total_product_count, UNSIGNED) + ${parseInt(
						item.products_buy_count
					)},
					tb1.cart_total_price = CONVERT(tb1.cart_total_price, UNSIGNED) + ${parseInt(
						item.total_price
					)},
					tb1.delivery_price = ${
						parseInt(price) +
							parseInt(item.total_price) -
							parseInt(item.total_discount_price) >
						43000
							? 0
							: 3000
					},
					tb1.total_cart_discount_price = CONVERT(tb1.total_cart_discount_price, UNSIGNED) + ${parseInt(
						item.total_discount_price
					)},
					tb1.total_accumulate_price = CONVERT(tb1.total_accumulate_price, UNSIGNED) + ${parseInt(
						item.total_accumulate_price
					)},
					tb1.payment_price = CONVERT(tb1.payment_price, UNSIGNED) + ${
						parseInt(item.total_price) - parseInt(item.total_discount_price)
					},
					tb1.update_dtm = now()
				WHERE tb1.user_seq = ${item.user_seq}
					AND tb1.status = "0";`
			);
			console.log("DB resultCartUpdate : ", resultCartUpdate.info);
			return resultCartUpdate.info;
		} catch (e) {
			console.error(e);
		}
	}
}

async function addToCart(item) {
	console.log("service item : ", item);
	if (item.guest_cart) {
		try {
			// 비회원 장바구니와 로그인시 장바구니 합하기 받은 배열내 데이터가 있을 때와
			// 없을 때를 구분 그리고 데이티거 있을 때 아래 코드와 호환이 될 수 있도록
			for await (let guest of item.guest_cart) {
				console.log("PPP: ", guest);
				const [cartDetail] = await dbPool.query(
					`SELECT tb1.*, tb2.cart_seq, tb2.payment_price
							 FROM tb_cart_detail tb1, tb_cart tb2
							WHERE tb2.user_seq = ${item.user_seq}
								AND tb1.product_seq = ${guest.product_seq}
								AND tb1.is_delete = "1"`
				);
				console.log("guest : cartDetail DB length: ", cartDetail.length);
				console.log("guest : cartDetail DB : ", cartDetail);
				if (!cartDetail.length) {
					console.log(">>> 없다");
					const [resultProduct] = await dbPool.query(
						`SELECT product_price, discount_price, accumulate_price, product_discount_price
							 FROM tb_product
							WHERE product_seq = ${guest.product_seq}`
					);
					console.log("guest product db result : ", resultProduct[0]);
					resultProduct[0].user_seq = item.user_seq;
					resultProduct[0].product_seq = guest.product_seq;
					resultProduct[0].product_view_seq = guest.product_view_seq;
					resultProduct[0].products_buy_count = guest.products_buy_count;
					resultProduct[0].total_price =
						parseInt(guest.products_buy_count) *
						parseInt(resultProduct[0].product_price);
					resultProduct[0].total_discount_price =
						parseInt(guest.products_buy_count) *
						parseInt(resultProduct[0].discount_price);
					resultProduct[0].total_accumulate_price =
						parseInt(guest.products_buy_count) *
						parseInt(resultProduct[0].accumulate_price);
					// 가져온 상품 금액들 item으로 만들어서 insertCart(item)호출하기 <<<<<<<<<
					const resultInsertCart = await insertCart(resultProduct[0]);
					console.log("resultInsertCart<><> : ", resultInsertCart);
					// const resultInsertCartDetail = await insertCartDetail(
					// 	item,
					// 	resultInsertCart
					// );
					const resultInsertCartDetail = await insertCartDetail(
						resultProduct[0],
						resultInsertCart
					);
					// return { cart: resultInsertCart, cartDetail: resultInsertCartDetail };
				} else {
					console.log("<<<있다");
					// 11/30 여기할 차례
					const resultUpdateCart = await updateCart(
						item,
						cartDetail[0].payment_price
					);
					const resultCartDetailUpdate = await updateCartDetail(
						item,
						cartDetail[0].cart_seq
					);
					console.log(">>>> :>", resultUpdateCart, resultCartDetailUpdate);
					return { cart: resultUpdateCart, cartDetail: resultCartDetailUpdate };
				}
			}
		} catch (e) {
			console.log(e);
		}
	} else {
		try {
			// 로그인상태에서 상품추가시
			const [cartDetail] = await dbPool.query(
				`SELECT tb1.*, tb2.cart_seq, tb2.payment_price
					 FROM tb_cart_detail tb1, tb_cart tb2
					WHERE tb2.user_seq = ${item.user_seq}
						AND tb1.product_seq = ${item.product_seq}
						AND tb1.is_delete = "1"`
			);
			console.log("cartDetail DB length: ", cartDetail.length);
			console.log("cartDetail DB : ", cartDetail);
			if (!cartDetail.length) {
				//console.log(">>> 없다");
				const resultInsertCart = await insertCart(item);
				console.log("resultInsertCart>> : ", resultInsertCart);
				const resultInsertCartDetail = await insertCartDetail(
					item,
					resultInsertCart
				);
				return { cart: resultInsertCart, cartDetail: resultInsertCartDetail };
			} else {
				//console.log("<<<있다");
				const resultUpdateCart = await updateCart(
					item,
					cartDetail[0].payment_price
				);
				const resultCartDetailUpdate = await updateCartDetail(
					item,
					cartDetail[0].cart_seq
				);
				console.log(">>>> :>", resultUpdateCart, resultCartDetailUpdate);
				return { cart: resultUpdateCart, cartDetail: resultCartDetailUpdate };
			}
		} catch (e) {
			console.log(e);
		}
	}
}

async function getCartList(user) {
	console.log("getCartList : ", user);
	try {
		const result = await dbPool.query(
			`SELECT 
				tb1.cart_seq, tb1.user_seq, tb1.total_product_count, tb1.status,
				tb1.cart_total_price, tb1.delivery_price, tb1.total_cart_discount_price,
				tb1.total_accumulate_price, tb1.payment_price,
				tb2.cart_detail_seq, tb2.product_view_seq, tb2.product_seq, 
				tb2.products_buy_count, tb2.products_total_price, tb2.is_delete, 
				tb3.product_name, tb3.product_price, tb3.product_stock, tb3.discount_price, tb3.product_status,
				tb4.product_view_title, tb4.vender, tb4.packaging_type, tb4.packaging_type_detail,
				tb6.user_address_seq, tb6.address, tb6.address_detail, 
				tb5.product_img_seq, tb5.product_img
			FROM tb_cart tb1
			INNER JOIN tb_cart_detail tb2
			ON tb1.cart_seq = tb2.cart_seq
			LEFT JOIN tb_product tb3
			ON tb2.product_seq = tb3.product_seq
			LEFT JOIN tb_product_view tb4
			ON tb2.product_seq = tb4.product_seq
			LEFT JOIN tb_product_img tb5
			ON tb2.product_seq = tb5.product_seq
			LEFT JOIN tb_user_address tb6
			ON tb1.user_seq = tb6.user_seq
			WHERE tb1.user_seq = "${user.user_seq}"
			AND tb2.is_delete = "1"
			AND tb3.product_status = "1"
			AND tb5.product_img_type = "0"
			AND tb6.default_address = "1"`
		);
		console.log("getCartList DB result : ", result[0]);
		return result[0];
	} catch (e) {
		console.log(e);
	}
}

async function deleteToCart(items) {
	console.log("deleteToCart : ", items.cart_detail_seq);
	const productCount = [];
	let sum = 0;
	try {
		for await (const seq of items.cart_detail_seq) {
			const result = await dbPool.query(
				`SELECT *
				 FROM tb_cart_detail tb
				WHERE tb.cart_detail_seq = "${seq}"
				  AND tb.is_delete = "1"`
			);
			productCount.push(result[0][0]);
			sum += parseInt(result[0][0].products_buy_count);
		}
		console.log("productCount : ", productCount, sum);
		const result = await dbPool.query(
			`SELECT total_product_count
			 FROM tb_cart tb
			WHERE tb.cart_seq = "${items.cart_seq}"`
		);
		const count = parseInt(result[0][0].total_product_count);
		console.log("resutl cart : ", count);
		if (count == sum) {
			const result = await dbPool.query(
				`DELETE tb1, tb2
				FROM tb_cart tb1
				LEFT JOIN tb_cart_detail tb2
				ON tb1.cart_seq = tb2.cart_seq
				WHERE tb1.cart_seq = "${items.cart_seq}"
				AND tb2.is_delete = "1"`
			);
			console.log("del : ", result[0]);
			return result[0].affectedRows;
		} else {
			const resultUpdate = await dbPool.query(
				`UPDATE tb_cart tb1
				JOIN tb_cart_detail tb2
				ON tb1.cart_seq = tb2.cart_seq
				SET tb1.total_product_count = CONVERT(tb1.total_product_count, UNSIGNED) - "${items.product_buy_count}",
					tb1.cart_total_price = CONVERT(tb1.cart_total_price, UNSIGNED) - "${items.product_total_price}",
					tb1.total_cart_discount_price = CONVERT(tb1.total_cart_discount_price, UNSIGNED) - "${items.discount_price}",
					tb1.total_accumulate_price = CONVERT(tb1.total_accumulate_price, UNSIGNED) - "${items.accumulate_price}",
					tb1.payment_price = CONVERT(tb1.payment_price, UNSIGNED) - "${items.product_total_price}",
					tb1.delivery_price = CASE WHEN CONVERT(tb1.payment_price, UNSIGNED) - "${items.product_total_price}" < 40000 THEN 3000 ELSE tb1.delivery_price END,
					tb1.update_dtm = now()
				WHERE tb2.cart_detail_seq = "${items.cart_detail_seq}"`
			);
			console.log("del up : ", resultUpdate[0]);
			const resultDel = await dbPool.query(
				`DELETE FROM tb_cart_detail
				WHERE cart_detail_seq = "${items.cart_detail_seq}"`
			);
			console.log("del del : ", resultDel[0]);
			return [resultUpdate[0].info, resultDel[0].affectedRows];
		}
	} catch (e) {
		console.log(e);
	}
}

async function updateToCart(item) {
	console.log("update : ", item);
	try {
		const result = await dbPool.query(
			`UPDATE tb_cart tb1
			JOIN tb_cart_detail tb2
			  ON tb1.cart_seq = tb2.cart_seq
			SET tb1.total_product_count = "${item.total_product_count}",
				tb1.cart_total_price = "${item.cart_total_price}",
				tb1.delivery_price = "${item.delivery_price}",
				tb1.total_cart_discount_price = "${item.total_cart_discount_price}",
				tb1.total_accumulate_price = "${item.total_accumulate_price}",
				tb1.payment_price = "${item.payment_price}",
				tb1.update_dtm = now(),
				tb2.products_buy_count = "${item.products_buy_count}",
				tb2.products_total_price = "${item.products_total_price}",
				tb2.update_dtm = now()
			WHERE tb1.cart_seq = "${item.cart_seq}"
				AND tb2.user_seq = "${item.user_seq}"
				AND tb2.product_seq = "${item.product_seq}"
				AND tb2.cart_detail_seq = "${item.cart_detail_seq}"`
		);
		console.log(result[0].info);
		return result[0].info;
	} catch (e) {
		console.log(e);
	}
}
// 품절확인
async function checkStock(user) {
	const [checkStock] = await dbPool.query(
		`SELECT tb1.product_seq, tb1.product_stock
		FROM tb_cart_detail tb2
		INNER JOIN tb_product tb1
		ON tb1.product_seq = tb2.product_seq
		WHERE tb2.user_seq = ${user.user_seq}
		AND tb2.is_delete = "1"
		ORDER BY tb1.product_stock ASC`
	);
	console.log("checkStock DB result : ", checkStock);
	return checkStock;
}

async function orderCart(user) {
	console.log("order db : ", user);
	try {
		const resultCheckStock = await checkStock(user);
		if (resultCheckStock[0].product_stock != "0") {
			const [order] = await dbPool.query(
				`SELECT
					tb1.user_name, tb1.user_phone, tb1.user_email,
					tb2.user_address_seq, tb2.zip_code,	tb2.address, tb2.address_detail,
					tb2.default_address, tb2.receiver, tb2.receiver_phone, tb2.receiver_place,
					tb2.receiver_place_etc, tb2.door_pass, tb2.arrival_message_time
				FROM tb_user tb1
				INNER JOIN tb_user_address tb2
				ON tb1.user_seq = tb2.user_seq
				WHERE tb1.user_seq = ${user.user_seq}
				AND tb2.default_address = "1"`
			);
			console.log("order db result : ", ...order);
			const [coupon] = await dbPool.query(
				`SELECT
					tb1.user_coupon_seq, tb1.coupon_seq,
					tb2.coupon_seq, tb2.category_seq, tb2.coupon_name, tb2.coupon_description,
					tb2.coupon_action, tb2.coupon_percent,	tb2.coupon_price, tb2.max_price
				FROM tb_user_coupon tb1
				INNER JOIN tb_coupon tb2
				ON tb1.coupon_seq = tb2.coupon_seq
				WHERE tb1.user_seq = ${user.user_seq}
				AND tb1.is_use = "0"`
			);
			console.log("coupon db result : ", coupon);
			const [accumulator] = await dbPool.query(
				`SELECT
					sum(accumulate_price) as accumulate_price
				FROM tb_accumulate
				WHERE user_seq = ${user.user_seq}
				AND is_use = "0"`
			);
			console.log("accumulator db result : ", ...accumulator);
			const [paymentMethod] = await dbPool.query(
				`SELECT payment_method, payment_kind, is_installment
				FROM tb_payment
				WHERE user_seq = ${user.user_seq}
				ORDER BY paymented_dtm DESC LIMIT 1;`
			);
			console.log("paymentMethod db result : ", paymentMethod);
			return { order, coupon, accumulator, paymentMethod };
		} else {
			return { message: "품절된 상품이 있습니다!", resultCheckStock };
		}
	} catch (e) {
		console.error(e);
	}
}

module.exports = {
	addToCart,
	getCartList,
	deleteToCart,
	updateToCart,
	orderCart,
};
