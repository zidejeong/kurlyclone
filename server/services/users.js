const dbPool = require("../db");

async function resetPw(user) {
	console.log("user >> : ", user);
	try {
		const result = await dbPool.query(
			`UPDATE tb_user 
      SET user_password = "${user.user_password}" 
      WHERE user_id = "${user.user_id}"`
		);
		// 수정한 정보반환
		return result[0].info;
	} catch (error) {
		console.error(error);
	}
}

async function checkedUser(user) {
	console.log("userId >> : ", Object.values(user)[0].length);
	if (Object.values(user)[0].length < 6) {
		return { message: "사용 할 수 없습니다." };
	} else {
		try {
			const result = await dbPool.query(
				`SELECT *
	      FROM tb_user
	      WHERE ${Object.keys(user)[0]} = "${Object.values(user)[0]}"`
			);
			if (result[0].length == 0) {
				return { message: "사용 할 수 없습니다." };
			} else {
				return { message: "사용 할 수 있습니다." };
			}
		} catch (error) {
			console.error(error);
		}
	}
}

async function checkedUserPassword(user) {
	console.log("user >> : ", Object.values(user)[1]);
	try {
		const result = await dbPool.query(
			`SELECT * 
      FROM tb_user 
      WHERE ${Object.keys(user)[0]} = "${Object.values(user)[0]}" 
      AND ${Object.keys(user)[1]} = "${Object.values(user)[1]}"`
		);
		console.log("result >> : ", result[0][0]);
		// DB에서 가져온 회원데이터에 메시지 넣기
		if (Object.keys(user)[0] == "user_name") {
			result[0][0].message = "아이디로 로그인하세요!";
			return result[0][0];
		} else {
			result[0][0].message = "새로운 비밀빈호를 입력하세요!";
			return result[0][0];
		}
	} catch (error) {
		console.error(error);
	}
}

async function findByAddress(user) {
	console.log("findByAddress : ", user);
	try {
		const result = await dbPool.query(
			`SELECT *
			FROM tb_user JOIN tb_user_address
			ON tb_user.user_seq = tb_user_address.user_seq
			WHERE tb_user.user_id = "${user.user_id}"`
		);
		console.log("findByAddress : ", result[0]);
		return result[0];
	} catch (error) {
		console.error(error);
	}
}

async function addUserAddress(user) {
	try {
		// 새 주소가 기본배송지인지 확인
		if (user.default_address == 1) {
			try {
				const result = dbPool.query(
					`UPDATE tb_user_address
					SET default_address = 0
					WHERE user_seq = "${user.user_seq}"`
				);
			} catch (error) {
				console.error(error);
			}
		}

		const result = await dbPool.query(
			`INSERT INTO tb_user_address SET
		      user_seq=?,
		      zip_code=?,
		      address=?,
		      address_detail=?,
		      default_address=?,
					create_dtm=?,
		      update_dtm=?`,
			[
				user.user_seq,
				user.zip_code,
				user.address,
				user.address_detail,
				user.default_address,
				new Date(),
				new Date(),
			]
		);

		return result[0].insertId;
	} catch (error) {
		console.error(error);
	}
}

async function delAddress(adr) {
	console.log("delAddress DB : ", adr);
	try {
		const result = await dbPool.query(
			`DELETE FROM tb_user_address
			WHERE user_address_seq = "${adr.seq}"`
		);
		console.log("delAddress DB result :", result);
		return result[0].affectedRows;
	} catch (e) {
		console.error(e);
	}
}

async function updateAddress(adr) {
	console.log("update DB : ", adr);
	try {
		if (adr.default_address == 1) {
			const [resultDefaultAddress] = await dbPool.query(
				`UPDATE tb_user_address SET
					default_address = 0
				WHERE user_seq = ${adr.user_seq}`
			);
			console.log("Default address set : ", resultDefaultAddress);
		}
		const [result] = await dbPool.query(
			`UPDATE tb_user_address SET 
				address_detail = "${adr.address_detail}",
				default_address = "${adr.default_address}",
				receiver = "${adr.receiver}",
				receiver_phone = "${adr.receiver_phone}",
				update_dtm = now()
			WHERE user_seq = ${adr.user_seq}
			AND user_address_seq = ${adr.user_address_seq}`
		);
		console.log("update result DB : ", result);
		return result.info;
	} catch (e) {
		console.error(e);
	}
}

async function getReciver(item) {
	console.log("getReceiver db : ", item);
	try {
		const [result] = await dbPool.query(
			`SELECT * FROM tb_user_address WHERE user_address_seq = ${item.user_address_seq}`
		);
		console.log("getReceiver db result : ", result[0]);
		return result[0];
	} catch (e) {
		console.error(e);
	}
}

async function saveReciver(info) {
	console.log("saveReceiver db : ", info);
	try {
		const [result] = await dbPool.query(
			`UPDATE tb_user_address SET
				receiver = "${info.receiver}",
				receiver_phone = "${info.receiver_phone}",
				receiver_place = "${info.receiver_place}",
				receiver_place_etc = "${info.receiver_place_etc}",
				door_pass = "${info.door_pass}",
				arrival_message_time = "${info.arrival_message_time}",
				update_dtm = now()
			WHERE user_address_seq = ${info.user_address_seq}
			AND user_seq = ${info.user_seq}`
		);
		console.log("saveReceiver db result : ", result);
		return result.info;
	} catch (e) {
		console.error(e);
	}
}

module.exports = {
	resetPw,
	checkedUser,
	checkedUserPassword,
	findByAddress,
	addUserAddress,
	delAddress,
	updateAddress,
	getReciver,
	saveReciver,
};
