//MainNavbar 전체 카테고리 목록

export const menuItems = [
  {
    title: "전체 카테고리",
    url: "/services",
    submenu: [
      {
        title: "채소",
        url: "/web-design",
        img: "",
        submenu: [
          {
            title: "친환경",
            url: "/frontend",
          },
          {
            title: "고구마·감자·당근",
            url: "",
          },
          {
            title: "시금치·쌈채소·나물",
            url: "",
          },
          {
            title: "브로콜리·파프리카·양배추",
            url: "",
          },
          {
            title: "양파·대파·마늘·배추",
            url: "",
          },
          {
            title: "오이·호박·고추",
            url: "",
          },
          {
            title: "냉동·이색·간편채소",
            url: "",
          },
          {
            title: "콩나물·버섯",
            url: "",
          },
        ],
      },
      {
        title: "과일·견과·쌀",
        url: "/web-dev",
        submenu: [
          {
            title: "친환경",
            url: "/frontend",
          },
          {
            title: "제철과일",
            url: "",
          },
          {
            title: "국산과일",
            url: "",
          },
        ],
      },
      {
        title: "수산·해산·건어물",
        url: "/seo",
      },
      {
        title: "정육·계란",
        url: "/seo",
      },
      {
        title: "국·반찬·메인요리",
        url: "/seo",
      },
      {
        title: "샐러드·간편식",
        url: "/seo",
      },
      {
        title: "면·양념·오일",
        url: "/seo",
      },
      {
        title: "생수·음료·우유·커피",
        url: "/seo",
      },
      {
        title: "간식·과자·떡",
        url: "/seo",
      },
      {
        title: "베이커리·치즈·델리",
        url: "/seo",
      },
      {
        title: "건강식품",
        url: "/seo",
      },
      {
        title: "와인",
        url: "/seo",
      },
      {
        title: "전통주",
        url: "/seo",
      },
      {
        title: "생활용품·리빙·캠핑",
        url: "/seo",
      },
      {
        title: "스킨케어·메이크업",
        url: "/seo",
      },
      {
        title: "헤어·바디·구강",
        url: "/seo",
      },
      {
        title: "주방용품",
        url: "/seo",
      },
      {
        title: "가전제품",
        url: "/seo",
      },
      {
        title: "반려동물",
        url: "/seo",
      },
      {
        title: "베이비·키즈·완구",
        url: "/seo",
      },
      {
        title: "컬리의 추천",
        url: "/seo",
      },
    ],
  },
  {
    title: "신상품",
    url: "/",
  },
  {
    title: "베스트",
    url: "/",
  },
  {
    title: "알뜰쇼핑",
    url: "/",
  },
  {
    title: "특가/혜택",
    url: "/",
  },
];
