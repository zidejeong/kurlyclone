import MainFooter from '../components/mainFooter';
import MainHeader from '../components/mainHeader';
import MainPageContent from '../components/mainPageContent';

function MainPage() {
  return (
    <div>
      <MainHeader />
      <MainPageContent />
      <MainFooter />
    </div>
  );
}
export default MainPage;
